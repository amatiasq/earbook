define(function(require) {
  'use strict';
  require('app-module');
  var angular = require('angular');
  angular.module('earbook')

  .controller('BookCtrl', function($scope) {
    // ...
  });
});
