define(function(require) {
  'use strict';
  require('app-module');
  var angular = require('angular');
  angular.module('earbook')

  .directive('mqMediaLoader', function() {
    return {
      //scope: { mqMediaLoader: '=' },
      link: function(scope, elem, attr) {
        var element = elem[0];

        console.log('pollas');
        scope.$watch(attr.mqMediaLoader, function(value, oldValue) {
          console.log('cacas', value, oldValue);

          if (oldValue) {
            oldValue.removeEventListener('progress', update);
            oldValue.removeEventListener('durationchange', update);
          }

          value.addEventListener('progress', update);
          value.addEventListener('durationchange', update);
          render(value);
        });


        function update(event) {
          render(event.target);
        }

        function render(media) {
          var buffer = media.buffered;
          var total = media.duration;
          var tracks = [];

          for (var i = 0; i < buffer.length; i++)
            tracks.push({ start: buffer.start(i), end: buffer.end(i) });

          console.log(tracks);

          tracks.forEach(function(track) {
            track.percentStart = Math.round(track.start / total * 10000) / 100;
            track.percentLoaded = Math.round((track.end - track.start) / total * 10000) / 100;
          });

          element.style.backgroundImage = tracks.map(function() {
            return '-webkit-linear-gradient(top, #dd0000, #e08585, #dd0000)';
          }).join(',');

          element.style.backgroundPosition = tracks.map(function(track) {
            return (100 / (100 - track.percentLoaded) * track.percentStart) + '% 0%';
          }).join(',');

          element.style.backgroundSize = tracks.map(function(track) {
            return track.percentLoaded + '% 100%';
          }).join(',');
        }
      }
    };
  });
});
