define(function(require) {

  function AudioWrapper(source) {
    this.dom = source instanceof Audio ? source : new Audio(source);
  }

  AudioWrapper.prototype = {
    constructor: AudioWrapper,

    get buffered() { return this.dom.buffered; },
    get duration() { return this.dom.duration; },
    //get ended() { return this.dom.ended; },
    //get readyState() { return this.dom.readyState; },
    //get seeking() { return this.dom.seeking; },
    get paused() { return this.dom.paused; },

    //get src() { return this.dom.src; },
    //set src(value) { this.dom.src = value; },
    //get muted() { return this.dom.muted; },
    //set muted(value) { this.dom.muted = value; },
    //get volume() { return this.dom.volume; },
    //set volume(value) { this.dom.volume = value; },
    get currentTime() { return Math.round(this.dom.currentTime * 10) / 10; },
    set currentTime(value) { this.dom.currentTime = value; },

    //canPlayType: function() { return this.dom.canPlayType(); },
    load: function() { return this.dom.load(); },
    play: function() { return this.dom.play(); },
    pause: function() { return this.dom.pause(); },

    addEventListener: function() {
      this.dom.addEventListener.apply(this.dom, arguments);
    },
    removeEventListener: function() {
      this.dom.addEventListener.apply(this.dom, arguments);
    }
  };

  return AudioWrapper;
});
