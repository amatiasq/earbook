define(function(require) {
  'use strict';
  require('app-module');
  var angular = require('angular');
  angular.module('earbook')

  .directive('mqRating', function() {
    return {
      restrict: 'AE',
      scope: { model: '=ngModel' },
      templateUrl: require.toUrl('./rating-directive.html'),

      link: function(scope) {
        scope.set = function(value) {
          scope.model = value;
        };
      }
    };
  });
});
