define(function(require) {
  'use strict';
  require('./progress-directive');
  require('./rating-directive');
  var Audio = require('./audio-wrapper');
  var angular = require('angular');
  angular.module('earbook')

  .controller('PlayerCtrl', function($scope) {
    var modifier = 10;
    var book = {
      title: 'Los Juegos del Hambre',
      chapters: [{
        title: '(24 - 55)',
        url: require.toUrl('sample-audio'),
      }]
    };

    /*book = {
      title: 'Foobar',
      chapters: [{
        title: 'Lorem ipsum',
        url: 'http://irishtimespubny.com/clocks.mp3',
      }]
    };*/

    $scope.book = book;
    $scope.chapter = book.chapters[0];
    $scope.audio = new Audio(book.chapters[0].url);

    $scope.back = function() {
      $scope.audio.currentTime -= modifier;
    };
    $scope.fwd = function() {
      $scope.audio.currentTime += modifier;
    };
  });
});
