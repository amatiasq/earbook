define(function(require) {
  'use strict';
  require('../time-filter');
  require('./media-loader-directive');
  require('app-module');
  var safeApply = require('../safe-apply');
  var angular = require('angular');
  angular.module('earbook')

  .directive('mqProgress', function() {
    return {
      restrict: 'AE',
      scope: { model: '=ngModel' },
      templateUrl: require.toUrl('./progress-directive.html'),

      compile: function(elem) {
        var element = elem[0];
        var slider = element.querySelector('.mq-progress-slider');
        var spacerLeft = element.querySelector('.mq-progress-spacer-left');
        var spacerRight = element.querySelector('.mq-progress-spacer-right');

        return function(scope) {
          scope.seek = function seek(time) {
            scope.model.currentTime = time;
          };

          scope.$watch('model', function(value, oldValue) {
            if (oldValue) {
              oldValue.removeEventListener('timeupdate', update);
              oldValue.removeEventListener('durationchange', update);
              oldValue.removeEventListener('loadedmetadata', update);
            }

            if (value) {
              value.addEventListener('timeupdate', update);
              value.addEventListener('durationchange', update);
              value.addEventListener('loadedmetadata', update);
              value.load();
              update();
            }
          });

          function update() {
            var media = scope.model;
            var duration = media.duration;
            var time = Math.round(media.currentTime);
            var percent = time / duration;
            var rest = 1 - percent;

            slider.style.width = (percent * 100) + '%';
            spacerLeft.style.flex = percent;
            spacerRight.style.flex = rest;
            scope.currentTime = time;
            safeApply(scope);
          }
        };
      }
    };
  });
});
