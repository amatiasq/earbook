define(function(require) {
  'use strict';
  require('app-module');
  require('comp/player/player');

  var angular = require('angular');
  angular.module('earbook')

  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'comp/player/player.html',
        controller: 'PlayerCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

  angular.bootstrap(document, [ 'earbook' ]);
});
